# AgoraWithVoicemod: Android

**Example Agora Marketplace app using the Voicemod for Agora Extension - Android**

Before you build the app, you must place the `voicemod-audio-filter-extension.aar` file in the `app/libs` folder, and you must place the required Agora library files in `app/src/main/jniLibs` (one subfolder per architecture):
- `libagora-rtc-sdk.so`
- `libagora-ffmpeg.so`
- `libagora_dav1d.so`

We have provided a convenience script (`fetch_agora_sdk.sh`) to simplify the process of fetching the Agora library files and placing them in the required subfolders.
