/* Copyright (C) Voicemod S.L. - All Rights Reserved
 * Unauthorized copying of this file, via any mean is strictly prohibited
 * This file is proprietary and confidential, and is subject to the legal notices
 * in its containing repository and at https://voicemod.net/copyright
 * For further information please email: copyright@voicemod.net
 */

package net.voicemod.agorapluginexample;

public class AppConf {
    public static final String appId = "";
    public static final String token = "";
    public final static String TAG = "Agora_zt java :";
    public final static String channelName = "vcmdchannel";
    public final static String apiKey = "";
    public final static String apiSecret = "";
}
