//
//  VideoChatViewController.m
//  Agora iOS Tutorial Objective-C
//
//  Created by James Fang on 7/15/16.
//  Copyright © 2016 Agora.io. All rights reserved.
//

#import "VideoChatViewController.h"
#import "AppID.h"

@interface VideoChatViewController() <AgoraMediaFilterEventDelegate, UIPickerViewDelegate, UIPickerViewDataSource> {
  NSString *faceInfo;
  NSString *handInfo;
  NSString *lightInfo;
  NSArray *_vcmdVoices;
}

@property (strong, nonatomic) AgoraRtcEngineKit *agoraKit;
@property (weak, nonatomic) IBOutlet UIView *localVideo;
@property (weak, nonatomic) IBOutlet UIView *remoteVideo;
@property (weak, nonatomic) IBOutlet UIView *controlButtons;
@property (weak, nonatomic) IBOutlet UIImageView *remoteVideoMutedIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *localVideoMutedBg;
@property (weak, nonatomic) IBOutlet UIImageView *localVideoMutedIndicator;
@property (weak, nonatomic) IBOutlet UITextView *infoTextView;
@property (assign, nonatomic) BOOL leavedChannel;
@property (weak, nonatomic) IBOutlet UIPickerView *vcmdVoicePicker;

@end

@implementation VideoChatViewController

- (void)viewDidLoad {
  [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [self setupButtons];
  [self startEngine];
}

- (void)startEngine {
  [self hideVideoMuted];
  [self initializeAgoraEngine];
  [self setupVideo];
  [self setupLocalVideo];
  [self joinChannel];
  [self enableEffect];
  [self fillVoiceUIList];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)initializeAgoraEngine {
  AgoraRtcEngineConfig *cfg = [AgoraRtcEngineConfig new];
  cfg.appId = appID;
  cfg.eventDelegate = self;
    
  self.agoraKit = [AgoraRtcEngineKit sharedEngineWithConfig:cfg delegate:self];
  [self.agoraKit enableExtensionWithVendor:@"Voicemod" extension:@"VoicemodExtension" enabled:YES];
}

- (void) fillVoiceUIList {
    _vcmdVoicePicker.delegate = self;
    _vcmdVoicePicker.dataSource = self;
        
    NSString *voices = [self.agoraKit getExtensionPropertyWithVendor:@"Voicemod" extension:@"VoicemodExtension" key:@"vcmd_presets"];
    _vcmdVoices =@[@"- none -"];
    if (voices){
        NSData *voicesData = [voices dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:voicesData options:NSJSONReadingMutableContainers error:nil];
        _vcmdVoices =[_vcmdVoices arrayByAddingObjectsFromArray:jsonArray];
    }
    [_vcmdVoicePicker reloadAllComponents];
    [self.view bringSubviewToFront:_vcmdVoicePicker];
}

- (void)setupVideo {
  // Default mode is disableVideo
  [self.agoraKit setChannelProfile:AgoraChannelProfileLiveBroadcasting];
  [self.agoraKit setClientRole:AgoraClientRoleBroadcaster];
  [self.agoraKit startPreview];
  // Set up the configuration such as dimension, frame rate, bit rate and orientation
  AgoraVideoEncoderConfiguration *encoderConfiguration =
  [[AgoraVideoEncoderConfiguration alloc] initWithSize:AgoraVideoDimension640x360
                                             frameRate:AgoraVideoFrameRateFps15
                                               bitrate:AgoraVideoBitrateStandard
                                       orientationMode:AgoraVideoOutputOrientationModeAdaptative mirrorMode:AgoraVideoMirrorModeAuto];
  [self.agoraKit setVideoEncoderConfiguration:encoderConfiguration];
}

- (void)setupLocalVideo {
  AgoraRtcVideoCanvas *videoCanvas = [[AgoraRtcVideoCanvas alloc] init];
  videoCanvas.uid = 0;
  // UID = 0 means we let Agora pick a UID for us
  
  videoCanvas.view = self.localVideo;
  videoCanvas.renderMode = AgoraVideoRenderModeHidden;
  [self.agoraKit setupLocalVideo:videoCanvas];
  [self.agoraKit enableLocalVideo:YES];
  [self.agoraKit enableLocalAudio:YES];
  [self.agoraKit enableVideo];
  [self.agoraKit enableAudio];
  // Bind local video stream to view
}

- (void)joinChannel {
  [self.agoraKit joinChannelByToken:token channelId:channelID info:nil uid:0 joinSuccess:^(NSString *channel, NSUInteger uid, NSInteger elapsed) {
      NSLog(@">> Channel joined %@", channel);
    // Join channel "demoChannel1"
  }];
  [self.agoraKit startPreview];
  // The UID database is maintained by your app to track which users joined which channels. If not assigned (or set to 0), the SDK will allocate one and returns it in joinSuccessBlock callback. The App needs to record and maintain the returned value as the SDK does not maintain it.
  
  [self.agoraKit setEnableSpeakerphone:YES];
  [UIApplication sharedApplication].idleTimerDisabled = YES;
}

/// Callback to handle the event such when the first frame of a remote video stream is decoded on the device.
/// @param engine - RTC engine instance
/// @param uid - user id
/// @param size - the height and width of the video frame
/// @param elapsed - lapsed Time elapsed (ms) from the local user calling JoinChannel method until the SDK triggers this callback.
- (void)rtcEngine:(AgoraRtcEngineKit *)engine firstRemoteVideoDecodedOfUid:(NSUInteger)uid size: (CGSize)size elapsed:(NSInteger)elapsed {
  if (self.remoteVideo.hidden) {
    self.remoteVideo.hidden = NO;
  }
  
  AgoraRtcVideoCanvas *videoCanvas = [[AgoraRtcVideoCanvas alloc] init];
  videoCanvas.uid = uid;
  // Since we are making a simple 1:1 video chat app, for simplicity sake, we are not storing the UIDs. You could use a mechanism such as an array to store the UIDs in a channel.
  
  videoCanvas.view = self.remoteVideo;
  videoCanvas.renderMode = AgoraVideoRenderModeHidden;
  [self.agoraKit setupRemoteVideo:videoCanvas];
  // Bind remote video stream to view
}

- (IBAction)hangUpButton:(UIButton *)sender {
  if (!self.leavedChannel) {
    [self leaveChannel];
    
    [UIView animateWithDuration:0.3 animations:^{
      self.remoteVideo.hidden = YES;
      self.localVideo.hidden = YES;
    } completion:^(BOOL completion){
      self.leavedChannel = YES;
    }];
  } else {
    [self joinChannel];
    [UIView animateWithDuration:0.3 animations:^{
      self.remoteVideo.hidden = NO;
      self.localVideo.hidden = NO;
    } completion:^(BOOL completion){
      self.leavedChannel = NO;
    }];
  }
}

///  Leave the channel and handle UI change when it is done.
- (void)leaveChannel {
  [self.agoraKit leaveChannel:^(AgoraChannelStats *stat) {
    [UIApplication sharedApplication].idleTimerDisabled = NO;
  }];
}

/// Callback to handle an user offline event.
/// @param engine - RTC engine instance
/// @param uid - user id
/// @param reason - why is the user offline
- (void)rtcEngine:(AgoraRtcEngineKit *)engine didOfflineOfUid:(NSUInteger)uid reason:(AgoraUserOfflineReason)reason {
  self.remoteVideo.hidden = true;
}

- (void)setupButtons {
  [self performSelector:@selector(hideControlButtons) withObject:nil afterDelay:3];
  UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(remoteVideoTapped:)];
  [self.view addGestureRecognizer:tapGestureRecognizer];
  self.view.userInteractionEnabled = true;
}

- (void)hideControlButtons {
  //self.controlButtons.hidden = true;
}

- (void)remoteVideoTapped:(UITapGestureRecognizer *)recognizer {
  if (self.controlButtons.hidden) {
    self.controlButtons.hidden = false;
    [self performSelector:@selector(hideControlButtons) withObject:nil afterDelay:3];
  }
}

- (void)resetHideButtonsTimer {
  [VideoChatViewController cancelPreviousPerformRequestsWithTarget:self];
  [self performSelector:@selector(hideControlButtons) withObject:nil afterDelay:3];
}

- (IBAction)didClickMuteButton:(UIButton *)sender {
  sender.selected = !sender.selected;
  [self.agoraKit muteLocalAudioStream:sender.selected];
  [self resetHideButtonsTimer];
}

- (IBAction)didClickVideoMuteButton:(UIButton *)sender {
  sender.selected = !sender.selected;
  [self.agoraKit muteLocalVideoStream:sender.selected];
  self.localVideo.hidden = sender.selected;
  self.localVideoMutedBg.hidden = !sender.selected;
  self.localVideoMutedIndicator.hidden = !sender.selected;
  [self resetHideButtonsTimer];
}

/// A callback to handle muting of the audio
/// @param engine  - RTC engine instance
/// @param muted  - YES if muted; NO otherwise
/// @param uid  - user id
- (void)rtcEngine:(AgoraRtcEngineKit *)engine didVideoMuted:(BOOL)muted byUid:(NSUInteger)uid {
  self.remoteVideo.hidden = muted;
  self.remoteVideoMutedIndicator.hidden = !muted;
}

- (void)hideVideoMuted {
  self.remoteVideoMutedIndicator.hidden = true;
  self.localVideoMutedBg.hidden = true;
  self.localVideoMutedIndicator.hidden = true;
}

- (IBAction)didClickSwitchCameraButton:(UIButton *)sender {
  sender.selected = !sender.selected;
  [self.agoraKit switchCamera];
  [self resetHideButtonsTimer];
}

- (void)enableEffect {
  static BOOL enabled = NO;
  if (enabled) {
      NSLog(@"Already anabled");
      return;
  }
  NSDictionary * userData = @{
          @"apiKey" : apiKey,
          @"apiSecret" : apiSecret
       };
  NSError *error = nil;
  NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userData options:NSJSONWritingPrettyPrinted error:&error];
  NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
  [self.agoraKit setExtensionPropertyWithVendor:@"Voicemod" extension:@"VoicemodExtension" key:@"vcmd_user_data" value:jsonString];
  enabled = YES;
}

- (IBAction)enableEffectTapped:(UIButton *)sender {
  [self enableEffect];
}

#pragma mark - AgoraMediaFilterEventDelegate

- (void)onEvent:(NSString * _Nullable)vendor
      extension:(NSString * _Nullable)extension
            key:(NSString * _Nullable)key
     json_value:(NSString * _Nullable)json_value {
}

#pragma mark - UIPickerViewDelegate/UIPickerViewDataSource

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_vcmdVoices count];
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [_vcmdVoices objectAtIndex:row];
}

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    [self enableEffect];
    NSString* voiceJson = nil;
    if (row == 0) {
        voiceJson = @"null";
    } else {
        voiceJson = [NSString stringWithFormat:@"\"%@\"" , [_vcmdVoices objectAtIndex:row]];
    }
    [self.agoraKit setExtensionPropertyWithVendor:@"Voicemod" extension:@"VoicemodExtension" key:@"vcmd_voice" value:voiceJson];
}

@end
