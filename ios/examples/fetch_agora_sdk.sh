#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TARGET_DIR=${SCRIPT_DIR}/Common

AGORA_SDK_VERSION=beta

AGORA_SDK_BASE_URL=https://download.agora.io/sdk/release
AGORA_SDK_FNAME_STABLE=Agora_Native_SDK_for_iOS_v3_6_0_1_FULL.zip
AGORA_SDK_FNAME_PREVIEW=Agora_Native_SDK_for_iOS_v4.0.0.preview.4_full.zip
AGORA_SDK_FNAME_PREVIEW_1=Agora_Native_SDK_for_iOS_v4.0.0.preview.3.1_full.zip
AGORA_SDK_FNAME_BETA=Agora_Native_SDK_for_iOS_v4.0.0-beta.1_FULL.zip

AGORA_SDK_SUBPATH=Agora_Native_SDK_for_iOS_FULL

if [[ "${AGORA_SDK_VERSION}" == "preview" ]]; then
	AGORA_SDK_FNAME=${AGORA_SDK_FNAME_PREVIEW}
elif [[ "${AGORA_SDK_VERSION}" == "beta" ]]; then
	AGORA_SDK_FNAME=${AGORA_SDK_FNAME_BETA}
else
	AGORA_SDK_FNAME=${AGORA_SDK_FNAME_STABLE}
fi

wget "${AGORA_SDK_BASE_URL}/${AGORA_SDK_FNAME}" -O ${SCRIPT_DIR}/sdk.zip
unzip ${SCRIPT_DIR}/sdk.zip -d ${SCRIPT_DIR} -x "Agora_Native_SDK_for_iOS_FULL/doc/*"

if [[ -d ${TARGET_DIR}/AgoraRtcKit.xcframework ]] || [[ -d ${TARGET_DIR}/Agoraffmpeg.xcframework ]] || [[ -d ${TARGET_DIR}/BeQuic.xcframework ]] || [[ -d ${TARGET_DIR}/AgoraDav1dExtension.xcframework ]] ; then
	echo ""
	read -p "Overwrite Agora xcframeworks [y]es [n]o (default: yes)?" sel
	if [[ "$sel" != "n" ]]; then
		rm -fr ${TARGET_DIR}/AgoraRtcKit.xcframework/
		rm -fr ${TARGET_DIR}/Agoraffmpeg.xcframework/
		rm -fr ${TARGET_DIR}/BeQuic.xcframework/
		rm -fr ${TARGET_DIR}/AgoraDav1dExtension.xcframework/
	fi
fi

if [[ ! -d ${TARGET_DIR}/AgoraRtcKit.xcframework ]]; then
	mv -v ${SCRIPT_DIR}/${AGORA_SDK_SUBPATH}/libs/Agoraffmpeg.xcframework ${TARGET_DIR}
	mv -v ${SCRIPT_DIR}/${AGORA_SDK_SUBPATH}/libs/AgoraRtcKit.xcframework ${TARGET_DIR}
	mv -v ${SCRIPT_DIR}/${AGORA_SDK_SUBPATH}/libs/BeQuic.xcframework ${TARGET_DIR}
	mv -v ${SCRIPT_DIR}/${AGORA_SDK_SUBPATH}/libs/AgoraDav1dExtension.xcframework ${TARGET_DIR}
else
	echo ""
	echo "No changes made to Agora xcframeworks"
fi

rm -fr ${SCRIPT_DIR}/${AGORA_SDK_SUBPATH}/
rm -f ${SCRIPT_DIR}/sdk.zip