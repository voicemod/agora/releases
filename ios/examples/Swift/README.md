# AgoraWithVoicemod: Swift

**Example Agora Marketplace app using the Voicemod for Agora Extension - iOS (Swift)**

Before you build the app, you must place the required framework packages in the `Frameworks` folder:
- `VoicemodAudioFilterExtension.xcframework`
- `AgoraAudioBeautyExtension.xcframework`
- `AgoraClearVisionExtension.xcframework`
- `AgoraContentInspectExtension.xcframework`
- `AgoraDav1d.xcframework`
- `Agorafdkaac.xcframework`
- `Agoraffmpeg.xcframework`
- `AgoraRtcKit.xcframework`
- `AgoraSoundTouch.xcframework`
- `AgoraSpatialAudioExtension.xcframework`
- `AgoraSuperResolutionExtension.xcframework`
- `AgoraVideoSegmentationExtension.xcframework`

We have provided convenience scripts to simplify the process of fetching the Agora framework packages and placing them in the required folder.

*`fetch_agora_sdk.sh`* will download the frameworks and place them in the `Common` folder.
*`copy_frameworks.sh`* will copy the frameworks `Frameworks` folders of both example projects (Swift and Objective-C).

You will have to manually place `VoicemodAudioFilterExtension.xcframework`, either in the `Common` folder before running the `copy_frameworks.sh` script, or directly in the `Frameworks` folder.
