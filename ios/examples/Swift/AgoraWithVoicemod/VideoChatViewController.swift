//
//  VideoChatViewController.swift
//  AgoraWithVoicemod
//
//  Created by Voicemod on 28/8/21.
//

import UIKit
import AgoraRtcKit

class VideoChatViewController: UIViewController, AgoraRtcEngineDelegate {

    @IBOutlet weak var localVideo: UIView!
    @IBOutlet weak var remoteVideo: UIView!
    @IBOutlet weak var controlButtons: UIView!
    @IBOutlet weak var remoteVideoMutedIndicator: UIImageView!
    @IBOutlet weak var localVideoMutedBg: UIImageView!
    @IBOutlet weak var localVideoMutedIndicator: UIImageView!

    @IBOutlet weak var infoTextView: UITextView!
    @IBOutlet weak var vcmdVoicePicker: UIPickerView!

    var vcmdVoices: [String] = []
    var agoraKit: AgoraRtcEngineKit!
    var leftChannel: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupButtons()
        startEngine()
    }
    
    func startEngine() {
        hideVideoMuted()
        initializeAgoraEngine()
        setupVideo()
        setupLocalVideo()
        joinChannel()
        enableEffect()
        fillVoicesList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initializeAgoraEngine() {
        let cfg = AgoraRtcEngineConfig()
        cfg.appId = appID
        cfg.eventDelegate = self
        
        self.agoraKit = AgoraRtcEngineKit.sharedEngine(with: cfg, delegate: self)
        self.agoraKit.enableExtension(withVendor: "Voicemod", extension: "VoicemodExtension", enabled: true)
    }
    
    func fillVoicesList() {
        vcmdVoicePicker.delegate = self;
        vcmdVoicePicker.dataSource = self;
        
        let voiceListJson = self.agoraKit.getExtensionProperty(withVendor: "Voicemod", extension: "VoicemodExtension", key: "vcmd_presets")
        if let jsonData = voiceListJson?.data(using: .utf8) {
            vcmdVoices = try! JSONDecoder().decode([String].self, from:jsonData)
        }
        vcmdVoices.insert("- none -",at:0)
        
        vcmdVoicePicker.reloadAllComponents()
        self.view.bringSubviewToFront(vcmdVoicePicker)
    }
    
    func setupVideo() {
        // Default mode is disableVideo
        self.agoraKit.setChannelProfile(.liveBroadcasting)
        self.agoraKit.setClientRole(.broadcaster)
        self.agoraKit.startPreview()
        // Set up the configuration such as dimension, frame rate, bit rate and orientation
        let encoderConfiguration = AgoraVideoEncoderConfiguration(
            size: AgoraVideoDimension640x360,
            frameRate: .fps15,
            bitrate: AgoraVideoBitrateStandard,
            orientationMode: .adaptative,
            mirrorMode: .auto)
        self.agoraKit.setVideoEncoderConfiguration(encoderConfiguration)

    }
    
    func setupLocalVideo() {
        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = 0
        // UID = 0 means we let Agora pick a UID for us
        
        videoCanvas.view = self.localVideo
        videoCanvas.renderMode = .hidden
        self.agoraKit.setupLocalVideo(videoCanvas)
        
        self.agoraKit.enableLocalAudio(true);
        self.agoraKit.enableLocalVideo(true);
        self.agoraKit.enableAudio();
        self.agoraKit.enableVideo();
    }
    
    func joinChannel() {
        self.agoraKit.joinChannel(byToken: token, channelId: channelID, info: nil, uid: 0) { channel, uid, elapsed in
            print(">> Channel joined", channel)
          // Join channel "demoChannel1"
        }
        self.agoraKit.startPreview()
        // The UID database is maintained by your app to track which users joined which channels. If not assigned (or set to 0), the SDK will allocate one and returns it in joinSuccessBlock callback. The App needs to record and maintain the returned value as the SDK does not maintain it.
        
        self.agoraKit.setEnableSpeakerphone(true)
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    /// Callback to handle the event such when the first frame of a remote video stream is decoded on the device.
    /// @param engine - RTC engine instance
    /// @param uid - user id
    /// @param size - the height and width of the video frame
    /// @param elapsed - lapsed Time elapsed (ms) from the local user calling JoinChannel method until the SDK triggers this callback.
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstRemoteVideoDecodedOfUid uid: UInt, size: CGSize, elapsed: Int) {

        if (self.remoteVideo.isHidden) {
            self.remoteVideo.isHidden = false
        }
      
        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = uid
        // Since we are making a simple 1:1 video chat app, for simplicity sake, we are not storing the UIDs. You could use a mechanism such as an array to store the UIDs in a channel.
        videoCanvas.view = self.remoteVideo
        videoCanvas.renderMode = .hidden
        self.agoraKit.setupRemoteVideo(videoCanvas)
        // Bind remote video stream to view
    }

    @IBAction func hangUpButton(sender: UIButton) {
        
        if (!self.leftChannel) {
            self.leaveChannel()

            UIView.animate(withDuration: 0.3, animations: {
                self.remoteVideo.isHidden = true
                self.localVideo.isHidden = true
            }, completion: { _ in
                self.leftChannel = true
            })
        } else {
            self.joinChannel()
            UIView.animate(withDuration: 0.3, animations: {
                self.remoteVideo.isHidden = false
                self.localVideo.isHidden = false
            }, completion: { _ in
                self.leftChannel = false
            })
        }
    }
    
    /// Leave the channel and handle UI change when it is done.
    func leaveChannel() {
        self.agoraKit.leaveChannel { _ in
            UIApplication.shared.isIdleTimerDisabled = false
        }
    }

    /// Callback to handle an user offline event.
    /// @param engine - RTC engine instance
    /// @param uid - user id
    /// @param reason - why is the user offline
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        self.remoteVideo.isHidden = true
    }

    func setupButtons() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.hideControlButtons()
        }
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(remoteVideoTapped))
        self.view.addGestureRecognizer(tapGestureRecognizer)
        self.view.isUserInteractionEnabled = true
    }

    func hideControlButtons() {
      //self.controlButtons.hidden = true
    }

    @objc func remoteVideoTapped(_ recognizer: UITapGestureRecognizer) {
        if (self.controlButtons.isHidden) {
            self.controlButtons.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.hideControlButtons()
            }
        }
    }

    func resetHideButtonsTimer() {
        VideoChatViewController.cancelPreviousPerformRequests(withTarget: self)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.hideControlButtons()
        }
    }

    @IBAction func didClickMuteButton(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.agoraKit.muteLocalAudioStream(sender.isSelected)
        self.resetHideButtonsTimer()
    }

    @IBAction func didClickVideoMuteButton(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.agoraKit.muteLocalVideoStream(sender.isSelected)
        self.localVideo.isHidden = sender.isSelected
        self.localVideoMutedBg.isHidden = !sender.isSelected
        self.localVideoMutedIndicator.isHidden = !sender.isSelected
        self.resetHideButtonsTimer()
    }

    /// A callback to handle muting of the audio
    /// @param engine  - RTC engine instance
    /// @param muted  - YES if muted; NO otherwise
    /// @param uid  - user id
    func rtcEngine(_ engine: AgoraRtcEngineKit, didVideoMuted muted: Bool, byUid uid: UInt) {
      self.remoteVideo.isHidden = muted
      self.remoteVideoMutedIndicator.isHidden = !muted
    }
    
    func hideVideoMuted() {
      self.remoteVideoMutedIndicator.isHidden = true
      self.localVideoMutedBg.isHidden = true
      self.localVideoMutedIndicator.isHidden = true
    }

    @IBAction func didClickSwitchCameraButton(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.agoraKit.switchCamera()
        self.resetHideButtonsTimer()
    }

    func enableEffect() {
        let userData = [
            "apiKey": apiKey,
            "apiSecret": apiSecret
        ]
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: userData, options: [.prettyPrinted])
            let jsonString = String(data: jsonData, encoding: .utf8)!
            self.agoraKit.setExtensionPropertyWithVendor("Voicemod", extension: "VoicemodExtension", key: "vcmd_user_data", value: jsonString)
        } catch {
            print("JSON serialization failed")
        }
    }
    
    @IBAction func enableEffectTapped(sender: UIButton) {
        self.enableEffect()
    }

}

extension VideoChatViewController: AgoraMediaFilterEventDelegate {

    func onEvent(_ vendor: String?, extension: String?, key: String?, value: String?) {
        
    }

}

extension VideoChatViewController: UIPickerViewDelegate, UIPickerViewDataSource {

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return vcmdVoices.count
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return vcmdVoices[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.enableEffect()
        let voiceJson = row == 0 ? "null" : String(format: "\"%@\"", vcmdVoices[row])
        self.agoraKit.setExtensionPropertyWithVendor("Voicemod", extension: "VoicemodExtension", key: "vcmd_voice", value: voiceJson)
    }

}
