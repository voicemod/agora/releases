// Copyright (C) Voicemod S.L. - All Rights Reserved
// Unauthorized copying of this file, via any mean is strictly prohibited
// This file is proprietary and confidential, and is subject to the legal notices
// in its containing repository and at https://voicemod.net/copyright
// For further information please email: copyright@voicemod.net


import Foundation

func trace(file: String = #file, line: Int = #line, function: String = #function) {
    print("[VCMD]:\(file):\(line) : \(function)")
}
