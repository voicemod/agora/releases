#!/bin/bash

set -e

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

rm -fr ${SCRIPTDIR}/Swift/Frameworks/*
rm -fr ${SCRIPTDIR}/Objective-C/Frameworks/*
cp -R ${SCRIPTDIR}/Common/* ${SCRIPTDIR}/Swift/Frameworks/
cp -R ${SCRIPTDIR}/Common/* ${SCRIPTDIR}/Objective-C/Frameworks/
