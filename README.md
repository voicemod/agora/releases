# Voicemod for Agora - Releases

Contains the Voicemod for Agora Extension library files, and example code for Agora Marketplace apps that use the extension.

To use the extension, please follow the instructions in the [documentation](https://voicemod.gitlab.io/agora/releases/).